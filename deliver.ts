import * as base64 from "https://deno.land/std@0.197.0/encoding/base64.ts";
import * as helpers from "npm:http-signature-helpers@1.0.2";

const createMessageText = await Deno.readTextFile("create-message.json");
const createMessage = JSON.parse(createMessageText);
const privatePem = await Deno.readTextFile("private.pem");
const privatePkcs8 = base64.decode(
  privatePem
    .replace(/^\s*-----BEGIN PRIVATE KEY-----\s+/, "")
    .replace(/\s+-----END PRIVATE KEY-----\s*$/, ""),
);
const privateKey = await crypto.subtle.importKey(
  "pkcs8",
  privatePkcs8,
  { name: "RSASSA-PKCS1-v1_5", hash: "SHA-256" },
  true,
  ["sign"],
);

const inboxUrl = new URL("/inbox", createMessage.object.inReplyTo);
const date = new Date();
const digest = await crypto.subtle.digest(
  "SHA-256",
  new TextEncoder().encode(createMessageText),
);
const headers = [
  ["Host", inboxUrl.host],
  ["Date", date.toUTCString()],
  ["Digest", `sha-256=${base64.encode(digest)}`],
];
const signatureHeaders = [
  "(request-target)",
  ...headers.map(([h]) => h.toLowerCase()),
];
const signatureString = helpers.default.getSignatureString({
  headers: Object.fromEntries(headers),
  signatureHeaders,
  target: {
    method: "POST",
    path: inboxUrl.pathname,
  },
});

const signature = await crypto.subtle.sign(
  "RSASSA-PKCS1-v1_5",
  privateKey,
  new TextEncoder().encode(signatureString),
);

const keyId: string =
  JSON.parse(await Deno.readTextFile("hongminhee.json")).publicKey.id;
headers.push([
  "Signature",
  `keyId="${keyId}",headers="${signatureHeaders.join(" ")}",signature="${
    base64.encode(signature)
  }"`,
]);

const response = await fetch(
  //"https://www.toptal.com/developers/postbin/1691557739404-7108767905738",
  inboxUrl.href,
  {
    method: "POST",
    headers,
    body: createMessageText,
  },
);

console.log(response);
console.log(await response.text());
