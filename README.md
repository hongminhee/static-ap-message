Static ActivityPub Message
==========================

This code is from reading and following Eugen Rochko's tutorial
[How to implement a basic ActivityPub server][1].

The actually sent message showed up from the remote server as below:

[![A message “수제 툿” (suje tut) from
@hongminhee@static-ap-message.netlify.app.](./screenshot.png)][2]

The message is in Korean, which means “handmade toot.”

[1]: https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/
[2]: https://elk.zone/seoul.earth/@hongminhee@static-ap-message.netlify.app/110865686079699932
